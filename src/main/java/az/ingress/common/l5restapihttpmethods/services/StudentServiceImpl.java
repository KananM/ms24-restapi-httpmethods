package az.ingress.common.l5restapihttpmethods.services;

import az.ingress.common.l5restapihttpmethods.domain.StudentEntity;
import az.ingress.common.l5restapihttpmethods.dto.StudentDto;
import az.ingress.common.l5restapihttpmethods.dto.StudentRequestDto;
import az.ingress.common.l5restapihttpmethods.exception.NotFoundException;
import az.ingress.common.l5restapihttpmethods.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public StudentDto create(StudentRequestDto requestDto) {
        StudentEntity student =
                StudentEntity
                        .builder()
                        .firstName(requestDto.getFirstName())
                        .lastName(requestDto.getLastName())
                        .age(requestDto.getAge())
                        .studentNumber(requestDto.getStudentNumber())
                        .build();
        final StudentEntity studentEntitySaved = studentRepository.save(student);

        return StudentDto
                .builder()
                .id(studentEntitySaved.getId())
                .firstName(studentEntitySaved.getFirstName())
                .lastName(studentEntitySaved.getLastName())
                .age(studentEntitySaved.getAge())
                .studentNumber(studentEntitySaved.getStudentNumber())
                .build();
    }

    @Override
    public StudentDto update(Long id, StudentRequestDto requestDto) {
        StudentEntity student =
                StudentEntity
                        .builder()
                        .id(id)
                        .firstName(requestDto.getFirstName())
                        .lastName(requestDto.getLastName())
                        .age(requestDto.getAge())
                        .studentNumber(requestDto.getStudentNumber())
                        .build();
        final StudentEntity studentEntitySaved = studentRepository.save(student);

        return StudentDto
                .builder()
                .id(studentEntitySaved.getId())
                .firstName(studentEntitySaved.getFirstName())
                .lastName(studentEntitySaved.getLastName())
                .age(studentEntitySaved.getAge())
                .studentNumber(studentEntitySaved.getStudentNumber())
                .build();
    }

    @Override
    public StudentDto get(Long id) {
        final StudentEntity studentEntity = studentRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        return StudentDto
                .builder()
                .id(studentEntity.getId())
                .firstName(studentEntity.getFirstName())
                .lastName(studentEntity.getLastName())
                .age(studentEntity.getAge())
                .studentNumber(studentEntity.getStudentNumber())
                .build();

    }

    @Override
    public void delete(Long id) {
        studentRepository.deleteById(id);

    }

    @Override
    public Page<StudentDto> list(@PageableDefault(size = 5, page = 1)
                                 Pageable pageable) {
        return studentRepository.findAll(pageable)
                .map(studentEntity -> StudentDto
                        .builder()
                        .id(studentEntity.getId())
                        .firstName(studentEntity.getFirstName())
                        .lastName(studentEntity.getLastName())
                        .age(studentEntity.getAge())
                        .studentNumber(studentEntity.getStudentNumber())
                        .build());


    }
}
