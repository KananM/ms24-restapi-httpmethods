package az.ingress.common.l5restapihttpmethods.services;

import az.ingress.common.l5restapihttpmethods.dto.StudentDto;
import az.ingress.common.l5restapihttpmethods.dto.StudentRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StudentService {
    StudentDto create(StudentRequestDto requestDto);

    StudentDto update(Long id, StudentRequestDto requestDto);

    StudentDto get(Long id);

    void delete(Long id);

    Page<StudentDto> list(Pageable page);
}
