package az.ingress.common.l5restapihttpmethods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class L5RestApiHttpMethodsApplication {

	public static void main(String[] args) {
		SpringApplication.run(L5RestApiHttpMethodsApplication.class, args);
	}

}
