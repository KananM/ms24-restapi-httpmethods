package az.ingress.common.l5restapihttpmethods.repository;

import az.ingress.common.l5restapihttpmethods.domain.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<StudentEntity, Long> {
}
