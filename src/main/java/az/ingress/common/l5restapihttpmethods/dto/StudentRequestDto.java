package az.ingress.common.l5restapihttpmethods.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentRequestDto {
    //    @NotNull   // null olmasın
    //    @NotEmpty // null ola biler amma bosh olmasin
    @NotBlank // ne null ola bilmez ne de bosh ola bilmez
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    private String studentNumber;
    @NotNull
    @Positive
    private Integer age;
}
