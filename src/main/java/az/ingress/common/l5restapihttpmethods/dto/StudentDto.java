package az.ingress.common.l5restapihttpmethods.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentDto {


    private Long id;
    private String firstName;
    private String lastName;
    private String studentNumber;
    private Integer age;
}
