package az.ingress.common.l5restapihttpmethods.rest;

import az.ingress.common.l5restapihttpmethods.dto.StudentDto;
import az.ingress.common.l5restapihttpmethods.dto.StudentRequestDto;
import az.ingress.common.l5restapihttpmethods.services.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    public Page<StudentDto> getStudent(Pageable page) {
       // log.info("Received page {} size {}", page);
        return studentService.list(page);
    }


    @GetMapping("/{id}")
    public StudentDto getStudent(@PathVariable Long id) {
        return studentService.get(id);

    }

    @PostMapping
    public StudentDto create(@RequestBody @Validated StudentRequestDto requestDto) {
        return studentService.create(requestDto);
    }

    @PutMapping("/{id}")
    public StudentDto update(
            @PathVariable Long id,
            @RequestBody StudentRequestDto requestDto) {

        return studentService.update(id, requestDto);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable Long id) {
        studentService.delete(id);
    }


}
